package com.why.zing.common.domain;

import lombok.Data;

/**
 * 请求日志
 *
 * @author 王洪玉
 * @date 2019/12/31
 */
@Data
public class WebLog {

    /**
     * 操作描述
     */
    private String description;



    /**
     * 操作时间
     */
    private Long startTime;


    /**
     * 消耗时间
     */
    private Integer spendTime;



    /**
     * URI
     */
    private String uri;


    /**
     * URL
     */
    private String url;


    /**
     * 请求类型
     */
    private String method;


    /**
     * 请求参数
     */
    private Object parameter;


    /**
     * 请求返回的结果
     */
    private Object result;
}
