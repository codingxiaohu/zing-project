package com.why.zing.common.exception;

import lombok.Getter;

/**
 * 系统异常父类
 *
 * @author 睁眼看世界
 * @date 2018/11/11
 */

@Getter
public class ServiceException extends ZingException {

    public ServiceException(ExceptionCode exceptionCode) {
        super(exceptionCode);
    }
}
