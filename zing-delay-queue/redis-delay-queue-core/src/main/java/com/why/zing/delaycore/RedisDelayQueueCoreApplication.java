package com.why.zing.delaycore;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
public class RedisDelayQueueCoreApplication {

    public static void main(String[] args) {
        SpringApplication.run(RedisDelayQueueCoreApplication.class, args);
    }

}
