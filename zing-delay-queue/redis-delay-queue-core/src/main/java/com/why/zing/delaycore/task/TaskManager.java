package com.why.zing.delaycore.task;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;
import java.util.function.Supplier;

import static com.why.zing.delaycore.task.SpeedTimeLogSuit.wrap;


/**
 * 任务执行管理
 *
 * @author 睁眼看世界
 * @date : 2020年1月16日
 */
@Slf4j
public class TaskManager {

    private TaskManager() {
    }

    /**
     * 创建一个可重用固定线程数的线程池
     */
    private static ExecutorService executorService = Executors.newFixedThreadPool(10);

    /**
     * 异步执行任务
     *
     * @param task  任务
     * @param title 标题
     */
    public static void doTask(final ITask task, String title) {
        executorService.execute(() -> wrap((Supplier<Void>) () -> {
            try {
                task.doTask();
            } catch (Exception e) {
                log.error("TaskManager doTask execute error.", e);
            }
            return null;
        }, title));
    }

    /**
     * 带有返回值的task
     *
     * @param task  任务
     * @param title 标题
     */
    public static <T> FutureTask<T> doFutureTask(final IFutureTask<T> task, String title) {
        FutureTask<T> future = new FutureTask<>(() -> wrap((task::doTask), title));
        executorService.execute(future);
        return future;
    }
}
